#!/usr/bin/env bash

# SPDX-FileCopyrightText: © 2022 Maik Herbers
# SPDX-License-Identifier: GPL-3.0-or-later

set -eo pipefail

print-help-and-exit() {
	cat <<-EOF
Usage:
  $BASH_ARGV0 <input-gram-matrix> <output-file>

Compute the automorphism group of a lattice.

Warning: The output depends on the basis!
EOF

	exit "$1"
}

if [[ "$1" = "-h" || "$1" = "--help" ]]; then
	print-help-and-exit 0
fi

if [[ "$#" -ne 2 ]]; then
	print-help-and-exit 1
fi

lattice_basis_file="$1"
output_file="$2"

lattice_basis="$(<"$lattice_basis_file" sed 's/^ *//; 1 s/^/[/; $ s/$/]/; s/ \+/, /g' | tr '\n' ';')"

tmpfile="$(mktemp)"
trap 'rm -- $tmpfile' SIGINT SIGTERM

gp -q >"$tmpfile" <<EOF
S = $lattice_basis;
print(matsize(S)[1]);
Aut = qfauto(S);
print(qfautoexport(Aut));
EOF

rank="$(head -n1 "$tmpfile")"

aut_group="$(tail -n +2 "$tmpfile")"

gap -q >"$tmpfile" <<EOF
Aut := List($aut_group);;
for f in Aut do PrintFormattedString(String(f));; Print("\n");; od;;
EOF

echo "$rank" >"$output_file"

while read -r line; do
	printf -- "--\n" >>"$output_file"
	printf '%s\n' "$line" | sed 'y/[,]/  \n/' | column -tR0 >>"$output_file"
done <"$tmpfile"

rm "$tmpfile"
