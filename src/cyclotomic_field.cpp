/* SPDX-FileCopyrightText: © 2022 Maik Herbers */
/* SPDX-License-Identifier: GPL-3.0-or-later */

#include "cyclotomic_field.hpp"

#include <numeric>

namespace weil
{
	namespace internal
	{
		static inline constexpr uint64_t
		modulus(int64_t a, int64_t b)
		{
			const int64_t res = a % b;
			return res >= 0 ? res : res + b;
		}
	}

	CyclotomicField::CyclotomicField()
	  : coeffs{ std::vector<mpq_class>(1) }
	{
	}

	CyclotomicField::CyclotomicField(uint64_t n, CyclotomicFieldInitType init)
	  : coeffs{ std::vector<mpq_class>(n ? n : 1) }
	{
		if (init == CyclotomicFieldInitType::IAmEigen) {
			this->coeffs[0] = n ? 1 : 0;
		}
	}

	CyclotomicField
	operator-(const CyclotomicField& a)
	{
		CyclotomicField res{ a };

		for (uint64_t i = 0; i < res.coeffs.size(); i++) {
			res.coeffs[i] *= -1_mpq;
		}

		return res;
	}

	CyclotomicField
	operator+(const CyclotomicField& a, const CyclotomicField& b)
	{
		CyclotomicField res{ std::lcm(a.coeffs.size(), b.coeffs.size()), CyclotomicFieldInitType::InitToZero };

		uint64_t stride_a{ res.coeffs.size() / a.coeffs.size() };
		uint64_t stride_b{ res.coeffs.size() / b.coeffs.size() };

		for (uint64_t i = 0; i < a.coeffs.size(); i++) {
			res.coeffs[stride_a * i] += a.coeffs[i];
		}

		for (uint64_t i = 0; i < b.coeffs.size(); i++) {
			res.coeffs[stride_b * i] += b.coeffs[i];
		}

		return res;
	}

	CyclotomicField
	operator*(const mpq_class& q, const CyclotomicField& a)
	{
		CyclotomicField res{ a.coeffs.size(), CyclotomicFieldInitType::InitToZero };

		for (uint64_t i = 0; i < a.coeffs.size(); i++) {
			res.coeffs[i] = q * a.coeffs[i];
		}

		return res;
	}

	CyclotomicField
	operator*(const CyclotomicField& a, const CyclotomicField& b)
	{
		CyclotomicField res{ std::lcm(a.coeffs.size(), b.coeffs.size()), CyclotomicFieldInitType::InitToZero };

		uint64_t stride_a{ res.coeffs.size() / a.coeffs.size() };
		uint64_t stride_b{ res.coeffs.size() / b.coeffs.size() };

		for (uint64_t i = 0; i < a.coeffs.size(); i++) {
			if (a.coeffs[i] == 0_mpq) {
				continue;
			}

			for (uint64_t j = 0; j < b.coeffs.size(); j++) {
				res.coeffs[(stride_a * i + stride_b * j) % res.coeffs.size()] +=
				  a.coeffs[i] * b.coeffs[j];
			}
		}

		return res;
	}

	CyclotomicField&
	CyclotomicField::operator+=(const CyclotomicField& other)
	{
		auto tmp{ *this + other };

		std::swap(this->coeffs, tmp.coeffs);

		return *this;
	}

	CyclotomicField&
	CyclotomicField::operator*=(const CyclotomicField& other)
	{
		auto tmp{ *this * other };

		std::swap(this->coeffs, tmp.coeffs);

		return *this;
	}

	void
	CyclotomicField::prettify()
	{
		if (this->coeffs.size() % 2 == 0) {
			/*
			 * we only want those roots with argument in [0, π)
			 *
			 * Let ξ ∈ ℂ be a n-th root of unity with n even.
			 * We use that
			 *           ξ^(i + n/2) = ξ^i * ξ^(n/2)
			 *                       = -ξ^i
			 * for all i ∈ ℤ.
			 */
			for (uint64_t i = 0; i < this->coeffs.size() / 2; i++) {
				this->coeffs[i] -= this->coeffs[i + this->coeffs.size() / 2];
				this->coeffs[i + this->coeffs.size() / 2] = 0;
			}
		} else if (this->coeffs.size() != 1) {
			/*
			 * get atleast one coefficient to zero
			 *
			 * Let ξ ∈ ℂ be a n-th root of unity with n > 1 odd.
			 * We use that
			 *           ∑_{i = 0}^{n} ξ^i = 0.
			 */
			uint64_t index_of_minimum{ 0 };

			for (uint64_t i = 1; i < this->coeffs.size(); i++) {
				if (abs(this->coeffs[index_of_minimum]) > abs(this->coeffs[i])) {
					index_of_minimum = i;
				}
			}

			auto min{ this->coeffs[index_of_minimum] };

			for (uint64_t i = 0; i < this->coeffs.size(); i++) {
				this->coeffs[i] -= min;
			}
		}

		/*
		 * cancel roots with rational real / imaginary part
		 *
		 * Let ξ ∈ ℂ be a root of unity.
		 * Then Re(ξ) ∈ ℚ if and only if
		 *           arg(ξ) / (2 π) ∈ {0, 1/6, 1/4, 1/3, 1/2, 2/3, 3/4, 5/6}.
		 * Similarly Im(ξ), as
		 *           Re(ξ) = cos(arg(ξ)),
		 *           Im(ξ) = sin(arg(ξ))
		 * and
		 *           sin(x)= cos(x - π/2)
		 * Im(ξ) ∈ ℚ iff
		 *           arg(ξ) / (2 π) ∈ {0, 1/12, 1/4, 5/12, 1/2, 7/12, 3/4, 11/12}.
		 *
		 * To simplify, we can use the fact that for ξ ∉ {-1, 1} with rational real part
		 *           Im(ξ + ξ^*) = 0
		 * and
		 *           Re(ξ + ξ^*) = 2 Re(ξ).
		 * For both we can consider pairs whose real / imaginary part cancels.
		 * They are:
		 *    Pairs whose imaginary part cancels
		 *           e(1/3) + e(2/3) = -1
		 *           e(1/6) - e(1/3) = 1.
		 *    Pairs whose real part cancels
		 *           e(1/12) + e(5/12) = i.
		 * All others cannot occour here, as the argument of at least
		 * one of them is bigger than π and for even n we only consider
		 * the roots with argument in [0, π).
		 */

		if (this->coeffs.size() % 3 == 0) {
			uint64_t third{ this->coeffs.size() / 3 };
			if (this->coeffs[third] == this->coeffs[2 * third]) {
				this->coeffs[0] -= this->coeffs[third];
				this->coeffs[third] = 0_mpq;
				this->coeffs[2 * third] = 0_mpq;
			}
		}
		if (this->coeffs.size() % 6 == 0) {
			uint64_t sixth{ this->coeffs.size() / 6 };
			if (this->coeffs[sixth] == -this->coeffs[2 * sixth]) {
				this->coeffs[0] += this->coeffs[sixth];
				this->coeffs[sixth] = 0_mpq;
				this->coeffs[2 * sixth] = 0_mpq;
			}
		}
		if (this->coeffs.size() % 12 == 0) {
			uint64_t twelveth{ this->coeffs.size() / 12 };
			if (this->coeffs[twelveth] == this->coeffs[5 * twelveth]) {
				this->coeffs[3 * twelveth] += this->coeffs[twelveth];
				this->coeffs[twelveth] = 0_mpq;
				this->coeffs[5 * twelveth] = 0_mpq;
			}
		}

		/*
		 * convert to smallest degree possible
		 *
		 * Let ξ ∈ ℂ be a primitive n-th root of unity.
		 * We factor out the gcd g of all positions with non-zero
		 * value. Then ξ^g is a primitive n/gcd(g, n)-th root of unity.
		 * Indeed, if k is the order of ξ, then clearly k | n.
		 * Now let r | n.
		 * Then
		 *           (ξ^g)^(n/r) = ξ^(g * n/r)
		 *                       = ξ^(g / r * n).
		 * As ξ is a primitive n-th root of unity, this equals 1 if and
		 * only if g/r ∈ ℤ, i.e. r | g. The order k of ξ is hence n/r
		 * where r is the largest integer with r | g and r | n, that is
		 * r = gcd(g, n).
		 * We therefore pull (only) k := n/gcd(g, n) out. If ξ is the
		 * primitive n-th root of unity with smallest argument, then
		 * ξ^gcd(g, n) is the primitive k-th root of unity with
		 * smallest argument.
		 */
		uint64_t g{ 0 };
		for (uint64_t i = 0; i < this->coeffs.size(); i++) {
			if (this->coeffs[i] != 0_mpq) {
				g = std::gcd(g, i);
			}
		}

		g = std::gcd(g, this->coeffs.size());
		if (g != 1) {
			std::vector<mpq_class> new_coeffs(this->coeffs.size() / g);

			for (uint64_t i = 0; i < new_coeffs.size(); i++) {
				new_coeffs[i] = this->coeffs[g * i];
			}

			std::swap(this->coeffs, new_coeffs);
		}
	}

	std::ostream&
	operator<<(std::ostream& o, const CyclotomicField& _z)
	{
		CyclotomicField z{ _z };
		z.prettify();

		bool first = true;
		for (uint64_t i = 0; i < z.coeffs.size(); i++) {
			if (z.coeffs[i] != 0_mpq) {
				if (i == 0) {
					o << z.coeffs[i];
				} else if (4 * i == z.coeffs.size()) {
					o << (first || z.coeffs[i] < 0 ? "" : "+");
					if (z.coeffs[i] == -1_mpq) {
						o << "-";
					} else if (z.coeffs[i] != 1_mpq) {
						o << z.coeffs[i] << "*";
					}
					o << "i";
				} else {
					o << (first || z.coeffs[i] < 0 ? "" : "+");
					if (z.coeffs[i] == -1_mpq) {
						o << "-";
					} else if (z.coeffs[i] != 1_mpq) {
						o << z.coeffs[i] << "*";
					}
					o << "e(" << mpq_class{ i, z.coeffs.size() } << ")";
				}

				first = false;
			}
		}

		if (first) {
			o << "0";
		}

		return o;
	}

	bool
	operator==(const CyclotomicField& a, const CyclotomicField& b)
	{
		auto res{ a - b };
		for (uint64_t i = 0; i < res.coeffs.size(); i++) {
			if (res.coeffs[i] != 0_mpq) {
				return false;
			}
		}

		return true;
	}

	CyclotomicField
	e_function(const mpq_class& s)
	{
		int64_t num{ mpz_get_si(s.get_num_mpz_t()) };
		uint64_t den{ mpz_get_ui(s.get_den_mpz_t()) };
		CyclotomicField res{ den, CyclotomicFieldInitType::InitToZero };
		res.coeffs[internal::modulus(num, den)] = 1_mpq;

		return res;
	}
}
