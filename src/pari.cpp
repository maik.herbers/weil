/* SPDX-FileCopyrightText: © 2022 Maik Herbers */
/* SPDX-License-Identifier: GPL-3.0-or-later */

#include "pari.hpp"

#include <pari/pari.h>

using weil::internal::operator<=>;

namespace weil::internal
{
	static mpz_class
	GEN_to_mpz(GEN d)
	{
		char* s{ GENtostr(d) };
		mpz_class res{ s };
		free(s);
		return res;
	}

	static GEN
	eigen_to_GEN(const mpz_matrix& m)
	{
		int64_t rows{ m.rows() };
		int64_t cols{ m.cols() };
		GEN m_pari = zeromatcopy(rows, cols);

		// Eigen and pari both use column-major storage
		for (int64_t j = 0; j < cols; j++) {
			for (int64_t i = 0; i < rows; i++) {
				GEN val{ gp_read_str(m(i, j).get_str().c_str()) };
				// indices start at 1
				gcoeff(m_pari, i + 1, j + 1) = val;
			}
		}

		return m_pari;
	}

	static GEN
	eigen_to_GEN(const mpq_matrix& m)
	{
		int64_t rows{ m.rows() };
		int64_t cols{ m.cols() };
		GEN m_pari = zeromatcopy(rows, cols);

		// Eigen and pari both use column-major storage
		for (int64_t j = 0; j < cols; j++) {
			for (int64_t i = 0; i < rows; i++) {
				GEN val{ gp_read_str(m(i, j).get_str().c_str()) };
				// indices start at 1
				gcoeff(m_pari, i + 1, j + 1) = val;
			}
		}

		return m_pari;
	}

	static mpz_matrix
	GEN_to_eigen(GEN m)
	{
		GEN size{ matsize(m) };
		char* rows_str = GENtostr(gel(size, 1));
		char* cols_str = GENtostr(gel(size, 2));
		int64_t rows{ std::strtoll(rows_str, nullptr, 10) };
		int64_t cols{ std::strtoll(cols_str, nullptr, 10) };
		free(rows_str);
		free(cols_str);

		mpz_matrix m_eigen{ rows, cols };

		for (int64_t j = 0; j < cols; j++) {
			for (int64_t i = 0; i < rows; i++) {
				m_eigen(i, j) = GEN_to_mpz(gcoeff(m, i + 1, j + 1));
			}
		}

		return m_eigen;
	}
}

namespace weil
{
	std::pair<mpz_vector, mpz_matrix>
	smith_normal_form(const mpz_matrix& m)
	{
		// 8 MiB
		pari_init(1 << 23, 0);

		GEN m_pari = internal::eigen_to_GEN(m);
		GEN V;
		GEN D{ ZM_snfall(m_pari, nullptr, &V) };

		// pari has the divisibility for elementary divisors flipped,
		// i.e. D_{i + 1, i + 1} | D_{i, i}
		// we therefore permute everything
		Eigen::PermutationMatrix<Eigen::Dynamic> P_left{ m.rows() };
		for (int64_t i = 0; i < P_left.rows(); i++) {
			P_left.indices().data()[i] = P_left.rows() - i - 1;
		}

		Eigen::PermutationMatrix<Eigen::Dynamic> P_right{ m.cols() };
		for (int64_t i = 0; i < P_right.cols(); i++) {
			P_right.indices().data()[i] = P_right.cols() - i - 1;
		}

		mpz_vector D_eigen{ P_right * internal::GEN_to_eigen(D).diagonal() };
		mpz_matrix V_eigen{ internal::GEN_to_eigen(V) * P_left };

		pari_close();

		return std::make_pair(D_eigen, V_eigen);
	}

	std::vector<mpz_vector>
	lattice_vectors_up_to_bound(const mpq_matrix& g, const mpq_class& bound)
	{
		// 1 GiB
		pari_init(1 << 30, 0);

		GEN g_pari{ internal::eigen_to_GEN(g) };
		// pari doesn't divide the bilinear product by two, i.e. q(v) = <v, v>
		// to work around this, just double the bound
		GEN bound_pari{ gp_read_str(mpq_class{ 2 * bound }.get_str().c_str()) };
		GEN res{ qfminim0(g_pari, bound_pari, nullptr, 2, 0) };

		char* num_str{ GENtostr(gel(res, 1)) };
		// pari only lists one of v and -v but reports both.
		// We thus have to divide by 2 (the zero vector is also omitted)
		uint64_t num{ std::strtoull(num_str, nullptr, 10) / 2 };
		free(num_str);

		GEN vectors_pari{ gel(res, 3) };
		std::vector<mpz_vector> vectors;
		vectors.reserve(num);

		mpz_matrix vectors_all{ internal::GEN_to_eigen(vectors_pari) };

		for (uint64_t i = 0; i < num; i++) {
			vectors.push_back(vectors_all.col(i));
		}

		pari_close();

		return vectors;
	}

	mpz_matrix
	lll_gram(const mpz_matrix& g)
	{
		// 8 MiB
		pari_init(1 << 23, 0);
		GEN g_pari{ internal::eigen_to_GEN(g) };
		GEN trans{ lllgramint(g_pari) };

		mpz_matrix trans_eigen{ internal::GEN_to_eigen(trans) };

		pari_close();

		return trans_eigen;
	}
}
