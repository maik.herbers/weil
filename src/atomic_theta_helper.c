/* SPDX-FileCopyrightText: © 2022 Maik Herbers */
/* SPDX-License-Identifier: GPL-3.0-or-later */

#include "atomic_theta_helper.h"

#include <stdatomic.h>
#include <stdlib.h>

struct power_series
{
	// ISO C defines no exact 64-bit atomic integers, so we use the "fast"
	// ones it does
	atomic_uint_fast64_t *coeffs;
};

struct theta_series
{
	uint64_t num_components;
	struct power_series* components;
};

static int init_power_series(struct power_series *p, uint64_t prec)
{
	// The exponents lie in the interval `[0, prec]', i.e. we need space
	// for `prec + 1' integers.
	p->coeffs = calloc(sizeof(atomic_uint_fast64_t), prec + 1);
	if (!p->coeffs) {
		return -1;
	}

	for (uint64_t i = 0; i <= prec; i++) {
		atomic_init(p->coeffs + i, 0);
	}

	return 0;
}

static void destroy_power_series(struct power_series p)
{
	free(p.coeffs);
}

void *init_theta_series(uint64_t num_components, uint64_t prec)
{
	struct theta_series *T = calloc(sizeof(struct theta_series), 1);
	if (!T) {
		return NULL;
	}

	T->num_components = num_components;

	T->components = calloc(sizeof(struct power_series), num_components);
	if (!T->components) {
		free(T);
		return NULL;
	}

	for (uint64_t i = 0; i < num_components; i++) {
		if (init_power_series(T->components + i, prec)) {
			for (uint64_t k = 0; k < i; k++) {
				destroy_power_series(T->components[k]);
			}

			free(T->components);
			free(T);

			return NULL;
		}
	}

	return T;
}

void destroy_theta_series(void *T_)
{
	struct theta_series *T = T_;
	for (uint64_t i = 0; i < T->num_components; i++) {
		destroy_power_series(T->components[i]);
	}

	free(T->components);
	free(T);
}

void atomic_theta_increment(void *T_, uint64_t comp, uint64_t exp)
{
	struct theta_series *T = T_;
	atomic_fetch_add(T->components[comp].coeffs + exp, 1);
}

uint64_t atomic_theta_read(void *T_, uint64_t comp, uint64_t exp)
{
	struct theta_series *T = T_;
	return atomic_load(T->components[comp].coeffs + exp);
}
