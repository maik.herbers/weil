/* SPDX-FileCopyrightText: © 2022 Maik Herbers */
/* SPDX-License-Identifier: GPL-3.0-or-later */

#include "discriminant_form.hpp"

#include "atomic_theta_helper.h"
#include "pari.hpp"

// import into the global namespace, so that it can be found.
// This is necessary, as `weil' is completely unrelated to `Eigen'
// and so this would not be found by ADL.
using weil::internal::operator<=>;

namespace weil
{
	using mpq_vector = Eigen::Matrix<mpq_class, Eigen::Dynamic, 1>;

	namespace internal
	{
		static mpz_class
		mod(mpz_class a, mpz_class b)
		{
			mpz_class ret;
			mpz_mod(ret.get_mpz_t(), a.get_mpz_t(), b.get_mpz_t());
			return ret;
		}

		static mpq_class
		mod1(mpq_class x)
		{
			mpq_class m;
			mpz_mod(m.get_num().get_mpz_t(), x.get_num().get_mpz_t(), x.get_den().get_mpz_t());
			m.get_den() = x.get_den();
			return m;
		}

		static mpz_vector
		reduce_vector(const mpz_vector& v, const mpz_vector& elem_divs)
		{
			mpz_vector ret{ v.rows() };
			for (int64_t i = 0; i < v.rows(); i++) {
				ret(i) = mod(v(i), elem_divs(i));
			}

			return ret;
		}

		static uint64_t
		vector_to_index(const mpz_vector& v, const mpz_vector& elem_divs)
		{
			mpz_matrix reduced{ reduce_vector(v, elem_divs) };
			mpz_class index = 0;
			mpz_class multiplier = 1;
			for (int64_t i = 0; i < reduced.rows(); i++) {
				index += multiplier * reduced(i);
				multiplier *= elem_divs(i);
			}

			return mpz_get_ui(index.get_mpz_t());
		}

		static mpz_vector
		project_into_discriminant_form(const mpz_vector& elem_divs, const mpq_matrix& v)
		{
			mpz_vector ret{ v.rows() };
			for (int64_t i = 0; i < v.rows(); i++) {
				mpq_class a{ elem_divs(i) * mod1(v(i)) };
				ret(i) = a.get_num();
			}

			return ret;
		}

		static mpz_vector
		apply_automorphism(const mpq_matrix& o,
				   const mpz_vector& v,
				   const mpz_vector& elem_divs,
				   const mpq_matrix& D_inv)
		{
			weil::mpq_vector v_lifted{ D_inv * v.cast<mpq_class>() };
			weil::mpq_vector mapped{ o * v_lifted };
			weil::mpz_vector projected{ project_into_discriminant_form(elem_divs, mapped) };

			return projected;
		}

		mpz_class
		mod_invert(mpz_class a, mpz_class b)
		{
			mpz_class res;
			mpz_invert(res.get_mpz_t(), a.get_mpz_t(), b.get_mpz_t());
			return res;
		}

		mpq_vector
		babai_near_vector(const mpq_matrix& trans, const mpq_matrix& trans_inv, const mpq_vector& v)
		{
			mpq_vector mapped{ trans_inv * v };
			mpq_vector rounded{ v.rows() };
			// round to nearest integer
			for (int64_t i = 0; i < v.rows(); i++) {
				mpq_class m{ internal::mod1(mapped(i)) };
				rounded(i) = mpz_class{ mapped(i) };
				if (mapped(i) >= 0) {
					if (m > 1_mpq / 2_mpq) {
						rounded(i) += 1_mpq;
					}
				} else {
					if (m < 1_mpq / 2_mpq) {
						rounded(i) -= 1_mpq;
					}
				}
			}

			return trans * rounded;
		}

	}

	void
	PowerSeries::increment(uint64_t idx)
	{
		if (this->coeffs.size() <= idx) {
			for (uint64_t i = this->coeffs.size(); i < idx; i++) {
				this->coeffs.push_back(0_mpz);
			}

			this->coeffs.push_back(1_mpz);
		} else {
			this->coeffs[idx]++;
		}
	}

	void
	format_powerseries(std::ostream& o, const PowerSeries& p, IOFormat style)
	{
		const char* start = "";
		const char* stop = "";
		const char* sep = "";
		const char* coeffs_start = "";
		const char* coeffs_stop = "";
		const char* coeffs_sep = "";

		const char* q_prefix = "";
		const char* q_suffix = "";
		const char* paren_start = "";
		const char* paren_stop = "";
		switch (style) {
		using enum IOFormat;
		case SageMath:
			start = "(";
			stop = ")";
			sep = ", ";
			coeffs_start = "[";
			coeffs_stop = "]";
			coeffs_sep = ", ";
			break;
		case PARI:
			start = "[";
			stop = "]";
			sep = ", ";
			coeffs_start = "[";
			coeffs_stop = "]";
			coeffs_sep = ", ";
			break;
		case Pretty:
			q_prefix = "q^(";
			q_suffix = ")";
			paren_start = "(";
			paren_stop = ")";
			goto start_printing;
		case LaTeX:
			q_prefix = "q^{";
			q_suffix = "}";
			paren_start = "\\left( ";
			paren_stop = " \\right)";
start_printing:
			if (p.offset != 0_mpq) {
				o << q_prefix << p.offset << q_suffix;
				o << paren_start;
			}

			bool first{ true };
			for (uint64_t i = 0; i < p.coeffs.size(); i++) {
				if (p.coeffs[i] == 0) {
					continue;
				}

				if (first) {
					first = false;
					o << p.coeffs[i];
				} else {
					o << (p.coeffs[i] >= 0 ? " + " : " - ");
					if (abs(p.coeffs[i]) != 1_mpq) {
						o << abs(p.coeffs[i]);
					} else if (i == 0) {
						o << "1";
					}
				}

				switch (i) {
				case 0: break;
				case 1:
					o << "q";
					break;
				default:
					o << q_prefix << i << q_suffix;
					break;
				}
			}

			if (first) {
				o << "0";
			}

			if (p.offset != 0_mpq) {
				o << paren_stop;
			}

			return;
		}

		o << start << p.offset << sep << coeffs_start;
		bool first{ true };
		for (uint64_t i = 0; i < p.coeffs.size(); i++) {
			if (first) {
				first = false;
			} else {
				o << coeffs_sep;
			}

			o << p.coeffs[i];
		}

		o << coeffs_stop << stop;
	}

	void
	format_vvthetaseries(std::ostream& o, const VVThetaSeries& T, IOFormat style)
	{
		std::vector<std::pair<mpz_vector, PowerSeries>> components;
		for (auto p : T.components) {
			components.push_back(p);
		}

		std::ranges::stable_sort(
		  components,
		  [](const PowerSeries& p1, const PowerSeries& p2) { return p1.offset < p2.offset; },
		  [](const auto& pair) { return pair.second; });

		const char* start = "";
		const char* stop = "";
		const char* prefix = "";
		const char* infix = "";
		const char* suffix = "";
		const char* sep = "";
		switch (style) {
		using enum IOFormat;
		case SageMath:
			start = "{\n";
			stop = "\n}";
			prefix = "";
			infix = ": ";
			suffix = "";
			sep = ",\n";
			break;
		case PARI:
			start = "[\\\n";
			stop = "\\\n]";
			prefix = "[";
			infix = ", ";
			suffix = "]";
			sep = ",\\\n";
			break;
		case Pretty:
			start = "";
			stop = "";
			prefix = "";
			infix = ": ";
			suffix = "";
			sep = "\n";
			break;
		case LaTeX:
			start = "";
			stop = "";
			prefix = "";
			infix = ": ";
			suffix = "";
			sep = " \\\\\n";
			break;
		}

		o << start;
		bool first{ true };
		for (const auto& [c, p] : components) {
			if (first) {
				first = false;
			} else {
				o << sep;
			}

			o << prefix;
			format_eigen(o, c, style);
			o << infix;
			format_powerseries(o, p, style);
			o << suffix;
		}
		o << stop;
	}

	DiscriminantForm::DiscriminantForm(const mpz_matrix& g)
	{
		auto&& [D, V] = smith_normal_form(g);

		mpq_matrix lifted_V{ V.cast<mpq_class>() };
		mpq_matrix lifted_D_inv{ D.cast<mpq_class>().asDiagonal().inverse() };
		this->gram_matrix =
		  (lifted_V * lifted_D_inv).transpose() * g.cast<mpq_class>() * (lifted_V * lifted_D_inv);
		this->elem_divs = D;

		mpz_class p = 1;
		for (int64_t i = 0; i < this->elem_divs.rows(); i++) {
			p *= this->elem_divs(i);
		}

		this->cardinality = p;
	}

	mpq_class
	DiscriminantForm::q(const mpz_vector& v) const
	{
		mpq_vector v_lifted{ v.cast<mpq_class>() };
		return internal::mod1(1_mpq / 2_mpq * v_lifted.transpose() * this->gram_matrix * v_lifted);
	}

	mpq_class
	DiscriminantForm::b(const mpz_vector& v, const mpz_vector& w) const
	{
		return internal::mod1(v.cast<mpq_class>().transpose() * this->gram_matrix * w.cast<mpq_class>());
	}

	VVThetaSeries
	DiscriminantForm::theta_series(uint64_t prec) const
	{
		// After factoring out q^offset, we want the highest power in
		// each component to be q^prec. We thus find vectors with norm
		// less than or equal to prec + 1 (i.e. those that appear in
		// the Fourier expansion), sort them into the appropriate
		// components and reject those with offset 0 and norm prec + 1.

		std::map<mpz_vector, PowerSeries> out;

		// Remember: This contains only one of `v' and `-v' and misses 0
		auto vectors{ lattice_vectors_up_to_bound(this->gram_matrix, mpq_class{ prec } + 1_mpq) };

		for (const auto& v : *this) {
			out[v].offset = this->q(v);
		}

		uint64_t uint_prec = mpz_get_ui(mpz_class{ prec }.get_mpz_t());

		auto atomic_theta{ init_theta_series(mpz_get_ui(this->cardinality.get_mpz_t()), uint_prec) };

#pragma omp parallel for
		for (uint64_t i = 0; i < vectors.size(); i++) {
			mpz_vector v{ vectors[i] };
			mpq_vector lifted{ v.cast<mpq_class>() };
			mpq_class norm{ (lifted.transpose() * this->gram_matrix * lifted).value() / 2_mpq };
			auto exp = mpz_get_ui(mpz_class{ norm - internal::mod1(norm) }.get_mpz_t());
			if (exp > prec) {
				continue;
			}

			uint64_t idx_1{ internal::vector_to_index(v, this->elem_divs) };
			uint64_t idx_2{ internal::vector_to_index(-v, this->elem_divs) };

			atomic_theta_increment(atomic_theta, idx_1, exp);
			atomic_theta_increment(atomic_theta, idx_2, exp);
		}

		// read coefficients back
		for (const auto& v : *this) {
			// Remember: The upper bound is inclusive
			for (uint64_t i = 0; i <= uint_prec; i++) {
				uint64_t c =
				  atomic_theta_read(atomic_theta, internal::vector_to_index(v, this->elem_divs), i);
				out[v].coeffs.push_back(c);
			}
		}

		destroy_theta_series(atomic_theta);

		// add zero
		mpz_vector v{ this->gram_matrix.rows() };
		v.setZero();
		out[v].increment(0);

		return VVThetaSeries{ out };
	}

	VVThetaSeries
	DiscriminantForm::theta_series_direct(uint64_t prec, const std::vector<mpz_matrix>& Aut) const
	{
		std::map<mpz_vector, PowerSeries> out;

		auto [orbits, _mapping] = this->get_orbits(Aut);

		const mpq_matrix D{ this->elem_divs.cast<mpq_class>().asDiagonal() };
		const mpq_matrix D_inv{ D.inverse() };
		const mpz_matrix g{ this->get_gram_wrt_smith_basis() };
		const mpq_matrix trans{ lll_gram(g).cast<mpq_class>() };
		const mpq_matrix trans_inv{ trans.inverse() };

		std::vector<mpq_vector> representatives;
		// find representatives of the shifted lattices with small norms
		for (const auto& o : orbits) {
			const mpq_matrix lifted_repr{ o.representative.cast<mpq_class>() };
			mpq_vector v{ -(D_inv * lifted_repr) };
			representatives.push_back(lifted_repr + D * internal::babai_near_vector(trans, trans_inv, v));
		}

		mpq_class max_offset_norm;
		for (uint64_t i = 0; i < representatives.size(); i++) {
			mpq_class norm{
				(representatives[i].transpose() * this->gram_matrix * representatives[i]).value() /
				2_mpq
			};
			if (norm > max_offset_norm) {
				max_offset_norm = norm;
			}
		}

		const mpq_matrix S{ g.cast<mpq_class>() };

		for (uint64_t i = 0; i < representatives.size(); i++) {
			out[orbits[i].representative].offset = this->q(orbits[i].representative);
		}

		// Similar to `theta_series', we want all vectors in the dual
		// lattice with norm less than prec + 1. To do this, we compute
		// all vectors in the lattice with norm less than or equal to
		//           2 (prec + max_offset_norm + 1).
		const auto vectors{ lattice_vectors_up_to_bound(
		  S, 2_mpq * (mpq_class{ prec } + max_offset_norm + 1_mpq)) };

		const mpq_class max_norm{ prec };
		auto atomic_theta{ init_theta_series(representatives.size(), prec) };

		for (uint64_t i = 0; i < representatives.size(); i++) {
			const mpq_vector offset{ D_inv * representatives[i] };

#pragma omp parallel for
			for (const auto& v : vectors) {
				const mpq_vector lifted{ v.cast<mpq_class>() };
				const mpq_vector lifted_1{ lifted + offset };
				const mpq_vector lifted_2{ -lifted + offset };
				const mpq_class norm_1{ (lifted_1.transpose() * S * lifted_1).value() / 2_mpq };
				const mpq_class norm_2{ (lifted_2.transpose() * S * lifted_2).value() / 2_mpq };

				if (norm_1 < max_norm + 1_mpq) {
					auto exp{ mpz_get_ui(
					  mpz_class{ norm_1 - internal::mod1(norm_1) }.get_mpz_t()) };
					atomic_theta_increment(atomic_theta, i, exp);
				}
				if (norm_2 < max_norm + 1_mpq) {
					auto exp{ mpz_get_ui(
					  mpz_class{ norm_2 - internal::mod1(norm_2) }.get_mpz_t()) };
					atomic_theta_increment(atomic_theta, i, exp);
				}
			}

			// `0' is not in `vectors', so we need to check `offset' manually
			mpq_class norm{ (offset.transpose() * S * offset).value() / 2_mpq };
			if (norm <= mpq_class{ prec }) {
				auto exp{ mpz_get_ui(mpz_class{ norm - internal::mod1(norm) }.get_mpz_t()) };
				atomic_theta_increment(atomic_theta, i, exp);
			}
		}

		for (uint64_t i = 0; i < representatives.size(); i++) {
			// Remember: The upper bound is inclusive
			for (uint64_t j = 0; j <= prec; j++) {
				uint64_t c = atomic_theta_read(atomic_theta, i, j);
				out[orbits[i].representative].coeffs.push_back(c);
			}
		}

		destroy_theta_series(atomic_theta);

		return VVThetaSeries{ out };
	}

	DiscriminantForm::Iterator::Iterator(mpz_class counter_, mpz_class max_, const mpz_vector& elem_divs_)
	  : elem_divs{ elem_divs_ }
	  , counter{ counter_ }
	  , max{ max_ }
	  , n{ elem_divs.rows() }
	  , v{ n }
	{
	}

	DiscriminantForm::Iterator::value_type
	DiscriminantForm::Iterator::operator*() const
	{
		return this->v;
	}

	DiscriminantForm::Iterator&
	DiscriminantForm::Iterator::operator++()
	{
		this->increment();
		return *this;
	}

	DiscriminantForm::Iterator&
	DiscriminantForm::Iterator::operator++(int)
	{
		this->increment();
		return *this;
	}

	bool
	DiscriminantForm::Iterator::operator==(const DiscriminantForm::Iterator& other) const
	{
		return this->counter == other.counter;
	}

	void
	DiscriminantForm::Iterator::increment()
	{
		// stop early when we are already finished
		if (this->counter == this->max) {
			return;
		}
		this->counter++;

		for (int64_t i = 0; i < this->n; i++) {
			if (this->v(i) + 1 != this->elem_divs(i)) {
				this->v(i)++;
				return;
			} else {
				this->v(i) = 0;
			}
		}
	}

	DiscriminantForm::Iterator
	DiscriminantForm::begin() const
	{
		return Iterator{ 0, this->cardinality, this->elem_divs };
	}

	DiscriminantForm::Iterator
	DiscriminantForm::end() const
	{
		return Iterator{ this->cardinality, this->cardinality, this->elem_divs };
	}

	std::set<mpz_vector>
	DiscriminantForm::c_kernel(const mpz_class& c) const
	{
		std::set<mpz_vector> ret;

		for (auto v : *this) {
			if (internal::reduce_vector(c * v, this->elem_divs).isZero()) {
				ret.insert(v);
			}
		}

		return ret;
	}

	std::set<mpz_vector>
	DiscriminantForm::c_image(const mpz_class& c) const
	{
		std::set<mpz_vector> ret;

		for (auto v : *this) {
			ret.insert(internal::reduce_vector(c * v, this->elem_divs));
		}

		return ret;
	}

	std::set<mpz_vector>
	DiscriminantForm::c_star(const mpz_class& c) const
	{
		std::set<mpz_vector> ret;

		auto c_kernel = this->c_kernel(c);

		for (auto alpha : *this) {
			bool include = true;
			for (auto gamma : c_kernel) {
				if (internal::mod1(c * this->q(gamma) + this->b(alpha, gamma)) != 0) {
					include = false;
					break;
				}
			}

			if (include) {
				ret.insert(alpha);
			}
		}

		return ret;
	}

	mpz_class
	DiscriminantForm::element_order(const mpz_vector& v) const
	{
		mpz_class order{ 1_mpz };
		for (int64_t i = 0; i < v.rows(); i++) {
			order = lcm(order, this->elem_divs(i) / gcd(v(i), this->elem_divs(i)));
		}

		return order;
	}

	mpz_class
	DiscriminantForm::level() const
	{
		mpz_class level{ 1_mpz };
		// the gram matrix is symmetric
		for (int64_t i = 0; i < this->gram_matrix.rows(); i++) {
			for (int64_t j = 0; j <= i; j++) {
				if (i != j) {
					level = lcm(level, this->gram_matrix(i, j).get_den());
				} else {
					bool even{ internal::mod(this->gram_matrix(i, j).get_num(), 2_mpz) == 0_mpz };
					level = lcm(level, (even ? 1_mpz : 2_mpz) * this->gram_matrix(i, j).get_den());
				}
			}
		}

		return level;
	}

	mpz_matrix
	DiscriminantForm::get_gram_wrt_smith_basis() const
	{
		mpq_matrix D{ this->elem_divs.cast<mpq_class>().asDiagonal() };
		return (D * this->gram_matrix * D).cast<mpz_class>();
	}

	std::pair<std::vector<Orbit>, std::map<mpz_vector, mpz_vector>>
	DiscriminantForm::get_orbits(const std::vector<mpz_matrix>& Aut) const
	{
		std::set<mpz_vector> remaining;
		for (auto&& v : *this) {
			remaining.insert(v);
		}

		mpq_matrix D_inv{ this->elem_divs.cast<mpq_class>().asDiagonal().inverse() };

		std::vector<Orbit> orbits;
		std::map<mpz_vector, mpz_vector> orbit_mapping;

		// we want iterate over remaining while we modify it
		// use this hack to pick one element
		while (!remaining.empty()) {
			for (auto v : remaining) {
				std::set<mpz_vector> orbit;
				orbit.insert(v);

				for (auto o : Aut) {
					mpz_vector mapped{ internal::apply_automorphism(
					  o.cast<mpq_class>(), v, this->elem_divs, D_inv) };
					orbit.insert(mapped);
					remaining.erase(mapped);
				}

				Orbit orbit_info{ v, orbit.size() };
				orbits.push_back(orbit_info);

				for (auto w : orbit) {
					orbit_mapping[w] = v;
				}

				remaining.erase(v);

				break;
			}
		}

		return std::make_pair(orbits, orbit_mapping);
	}

	mpz_vector
	DiscriminantForm::xc(const mpz_class& c) const
	{
		// FIXME: wrong, but works for my lattice
		// Strömberg 2011 (https://doi.org/10.48550/arXiv.1108.0202),
		// Lemma 2.4 (should be `unique element in x_c \in D_c', not `D')
		const auto c_kernel{ this->c_kernel(c) };
		const auto c_star{ this->c_star(c) };
		const auto c_image{ this->c_image(c) };
		for (auto candidate : c_kernel) {
			// Check that `candidate + D^c ⊆ D^c*'. This is
			// sufficient, as `+ candidate' is a bijection and D is
			// finite
			for (auto v : c_image) {
				if (!c_star.contains(internal::reduce_vector(candidate + v, this->elem_divs))) {
					goto next_round;
				}
			}

			return candidate;

		next_round:
			continue;
		}

		throw std::logic_error("xc not found");
	}

	mpq_class
	DiscriminantForm::qc(const mpz_vector& v, const mpz_class& c, const mpz_vector& xc) const
	{
		// subtract xc, find preimage under `c *'
		weil::mpz_vector g{ v.rows() };
		for (int64_t i = 0; i < v.rows(); i++) {
			mpz_class c_inv{ internal::mod_invert(c, this->elem_divs(i)) };
			g(i) = internal::mod(c_inv * (v(i) - xc(i)), this->elem_divs(i));
		}

		return internal::mod1(c * this->q(g) + this->b(xc, g));
	}

	CyclotomicField_matrix
	DiscriminantForm::transformation_matrix(const std::vector<mpz_matrix>& Aut, const SL2Z& M) const
	{
		auto [orbits, orbit_mapping] = this->get_orbits(Aut);

		std::vector<mpz_vector> basis;
		for (auto orbit : orbits) {
			basis.push_back(orbit.representative);
		}

		mpz_class a{ M(0, 0) };
		mpz_class b{ M(0, 1) };
		mpz_class c{ M(1, 0) };
		mpz_class d{ M(1, 1) };

		// TODO: Also compare lexicographically, just to be sure?
		std::ranges::stable_sort(
		  basis, [this](const mpz_vector& v, const mpz_vector& w) { return this->q(v) < this->q(w); });

		const auto c_star{ this->c_star(c) };

		auto get_basis_index = [&basis, &orbit_mapping](const mpz_vector& v) -> int64_t {
			// TODO: this could be better, as `basis' is sorted (by norm and lexicographically as we use
			// `stable_sort' and we get the vectors out of `orbits' this way)
			return std::ranges::distance(basis.cbegin(), std::ranges::find(basis, orbit_mapping[v]));
		};

		Eigen::Matrix<CyclotomicField, Eigen::Dynamic, Eigen::Dynamic> transformation_matrix{ basis.size(),
												      basis.size() };

		mpz_vector xc{ this->xc(c) };

		// TODO: this should be easy and safe to parallelize, as each
		// thread would write to its own column
		for (uint64_t i = 0; i < basis.size(); i++) {
			mpz_vector g{ basis[i] };

			for (auto v : c_star) {
				auto coeff{ e_function(-a * this->qc(v, c, xc) - b * this->b(g, v) -
						       d * b * this->q(g)) };

				auto idx{ get_basis_index(internal::reduce_vector(d * g + v, this->elem_divs)) };
				transformation_matrix(idx, i) += coeff;
			}
		}

		return transformation_matrix;
	}
}
