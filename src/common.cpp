/* SPDX-FileCopyrightText: © 2022 Maik Herbers */
/* SPDX-License-Identifier: GPL-3.0-or-later */

#include "common.hpp"

namespace weil::internal
{
	std::strong_ordering
	operator<=>(const mpz_vector& a, const mpz_vector& b)
	{
		auto res = a.rows() <=> b.rows();
		if (res != 0) {
			return res;
		}

		for (int64_t i = 0; i < a.rows(); i++) {
			if (a(i) < b(i)) {
				return std::strong_ordering::less;
			} else if (a(i) > b(i)) {
				return std::strong_ordering::greater;
			}
		}

		return std::strong_ordering::equal;
	}
}

namespace weil
{
	void
	format_eigen(std::ostream& o, const mpz_vector& v, IOFormat style)
	{
		Eigen::IOFormat fmt;
		switch (style) {
		using enum IOFormat;
		case Pretty:
			fmt = {
				Eigen::StreamPrecision,
				Eigen::DontAlignCols,
				"",
				" ",
				"", "",
				"(", ")"
			};
			break;
		case SageMath:
			fmt = {
				Eigen::StreamPrecision,
				Eigen::DontAlignCols,
				"",
				", ",
				"", "",
				"(", ")"
			};
			break;
		case PARI:
			fmt = {
				Eigen::StreamPrecision,
				Eigen::DontAlignCols,
				"",
				"; ",
				"", "",
				"[", "]"
			};
			break;
		case LaTeX:
			fmt = {
				Eigen::StreamPrecision,
				Eigen::DontAlignCols,
				"",
				" & ",
				"", "",
				"\\begin{pmatrix} ", " \\end{pmatrix}",
			};
			break;
		}

		o << v.format(fmt);
	}
}
