/* SPDX-FileCopyrightText: © 2022 Maik Herbers */
/* SPDX-License-Identifier: GPL-3.0-or-later */

#include "sl2z.hpp"

namespace weil
{
	std::optional<std::vector<std::pair<SL2ZGenerator, mpz_class>>>
	split_into_generators(const SL2Z& m_)
	{
		// See Diamond--Shurman "A First Course in Modular Forms", Exercise 1.1.1

		SL2Z S;
		// clang-format off
		S << 0, -1,
		     1,  0;
		// clang-format on

		SL2Z m{ m_ };
		bool invertible;
		mpz_class det;

		// we need to check the determinant, as gmp will happily round integers
		m_.computeInverseAndDetWithCheck(m, det, invertible);
		if (abs(det) != 1) {
			return std::nullopt;
		}

		std::vector<std::pair<SL2ZGenerator, mpz_class>> split;

		while (m(1, 0) != 0) {
			mpz_class n = -m(1, 1) / m(1, 0);
			if (2 * abs(n * m(1, 0) + m(1, 1)) > abs(m(1, 0))) {
				// correct?
				if (n < 0) {
					n--;
				} else {
					n++;
				}
			}

			// clang-format off
			m << m(0, 0),   n * m(0, 0) + m(0, 1),
			     m(1, 0),   n * m(1, 0) + m(1, 1);
			// clang-format on

			if (n != 0) {
				split.push_back({ SL2ZGenerator::T, n });
			}

			m *= S;

			split.push_back({ SL2ZGenerator::S, 1 });
		}

		if (m(1, 1) == -1) {
			m *= S * S;
			split.push_back({ SL2ZGenerator::S, 2 });
		}

		mpz_class n = -m(0, 1);
		if (n != 0) {
			split.push_back({ SL2ZGenerator::T, n });
		}

		return split;
	}
}
