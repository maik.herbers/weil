/* SPDX-FileCopyrightText: © 2022 Maik Herbers */
/* SPDX-License-Identifier: GPL-3.0-or-later */

#pragma once

#include <optional>
#include <vector>

#include "common.hpp"

namespace weil
{
	using SL2Z = Eigen::Matrix<mpz_class, 2, 2>;

	enum class SL2ZGenerator {
		T,
		S,
	};

	/*
	 * Returns the sequence of matrices `T' and `S' with corresponding powers
	 * that have product `m_'.
	 * While interating the forward, the matrices have to be multiplied on
	 * the right side.
	 */
	std::optional<std::vector<std::pair<SL2ZGenerator, mpz_class>>> split_into_generators(const SL2Z& m);
}
