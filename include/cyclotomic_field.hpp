/* SPDX-FileCopyrightText: © 2022 Maik Herbers */
/* SPDX-License-Identifier: GPL-3.0-or-later */

#pragma once

#include "common.hpp"

namespace weil
{
	enum class CyclotomicFieldInitType {
		// Eigen wants to construct zero and non-zero elements (i.e. 0 and 1)
		IAmEigen,
		// this is for regular use
		InitToZero,
	};

	struct CyclotomicField {
		CyclotomicField();
		CyclotomicField(uint64_t n, CyclotomicFieldInitType init = CyclotomicFieldInitType::IAmEigen);

		void prettify();

		friend CyclotomicField operator-(const CyclotomicField& a);
		friend CyclotomicField operator+(const CyclotomicField& a, const CyclotomicField& b);
		friend CyclotomicField operator*(const mpq_class& q, const CyclotomicField& a);
		inline friend CyclotomicField
		operator-(const CyclotomicField& a, const CyclotomicField& b)
		{
			return a + (-b);
		}

		inline friend CyclotomicField
		operator*(const CyclotomicField& a, const mpq_class& q)
		{
			return q * a;
		}

		friend CyclotomicField operator*(const CyclotomicField& a, const CyclotomicField& b);

		CyclotomicField& operator+=(const CyclotomicField& other);
		CyclotomicField& operator*=(const CyclotomicField& other);

		friend std::ostream& operator<<(std::ostream& o, const CyclotomicField& _z);

		std::vector<mpq_class> coeffs;
	};

	bool operator==(const CyclotomicField& a, const CyclotomicField& b);

	CyclotomicField e_function(const mpq_class& s);
}

namespace Eigen
{
	template<>
	struct NumTraits<weil::CyclotomicField> {
		typedef weil::CyclotomicField Real;
		typedef weil::CyclotomicField NonInteger;
		typedef weil::CyclotomicField Literal;
		typedef weil::CyclotomicField Nested;

		enum {
			IsComplex = 0,
			IsInteger = 0,
			ReadCost = Eigen::HugeCost,
			AddCost = Eigen::HugeCost,
			MulCost = Eigen::HugeCost,
			IsSigned = 1,
			RequireInitialization = 1,
		};

		static int
		digits10()
		{
			return 0;
		}
	};
}
