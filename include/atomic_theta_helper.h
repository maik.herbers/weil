/* SPDX-FileCopyrightText: © 2022 Maik Herbers */
/* SPDX-License-Identifier: GPL-3.0-or-later */

#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void *init_theta_series(uint64_t num_components, uint64_t prec);
void destroy_theta_series(void *T);

void atomic_theta_increment(void *T, uint64_t comp, uint64_t exp);
uint64_t atomic_theta_read(void *T_, uint64_t comp, uint64_t exp);

#ifdef __cplusplus
}
#endif
