/* SPDX-FileCopyrightText: © 2022 Maik Herbers */
/* SPDX-License-Identifier: GPL-3.0-or-later */

#pragma once

#include "common.hpp"

#include <vector>

namespace weil
{
	/*
	 * Returns the matrices `D' and `V' in the smith normal form
	 *     D = U m V.
	 */
	std::pair<mpz_vector, mpz_matrix> smith_normal_form(const mpz_matrix& m);

	/*
	 * Returns the non-zero vectors in the lattice given by the
	 * gram matrix `g' that have norm no more than bound. Only one
	 * of `v' and `-v' is given.
	 */
	std::vector<mpz_vector>
	lattice_vectors_up_to_bound(const mpq_matrix& g, const mpq_class& bound);

	/*
	 * Returns a change of basis matrix from an LLL-reduced basis to the standard basis.
	 */
	mpz_matrix lll_gram(const mpz_matrix& g);
}
