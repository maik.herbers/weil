/* SPDX-FileCopyrightText: © 2022 Maik Herbers */
/* SPDX-License-Identifier: GPL-3.0-or-later */

namespace weil
{
	template<typename T>
	void
	format_eigen(std::ostream& o, const T& m, IOFormat style)
	requires
	     std::same_as<mpz_matrix, T>
	  || std::same_as<mpq_matrix, T>
	  || std::same_as<CyclotomicField_matrix, T>
	{
		Eigen::IOFormat fmt;
		switch (style) {
		using enum IOFormat;
		case Pretty: break;
		case SageMath:
			fmt = {
				Eigen::StreamPrecision,
				Eigen::DontAlignCols,
				", ",
				",\n",
				"[", "]",
				"matrix([", "])"
			};
			break;
		case PARI:
			fmt = {
				Eigen::StreamPrecision,
				Eigen::DontAlignCols,
				", ",
				";\\\n",
				"", "",
				"[", "]"
			};
			break;
		case LaTeX:
			fmt = {
				Eigen::StreamPrecision,
				Eigen::DontAlignCols,
				" & ",
				" \\\\\n",
				"", "",
				"\\begin{pmatrix}\n", "\n\\end{pmatrix}",
			};
			break;
		}

		o << m.format(fmt);
	}
}
