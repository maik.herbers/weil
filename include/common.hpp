/* SPDX-FileCopyrightText: © 2022 Maik Herbers */
/* SPDX-License-Identifier: GPL-3.0-or-later */

#pragma once

#include <compare>

#include <Eigen/Eigen>
#include <gmpxx.h>

namespace weil
{
	struct CyclotomicField;

	using mpz_matrix = Eigen::Matrix<mpz_class, Eigen::Dynamic, Eigen::Dynamic>;
	using mpq_matrix = Eigen::Matrix<mpq_class, Eigen::Dynamic, Eigen::Dynamic>;
	using CyclotomicField_matrix = Eigen::Matrix<CyclotomicField, Eigen::Dynamic, Eigen::Dynamic>;
	using mpz_vector = Eigen::Matrix<mpz_class, Eigen::Dynamic, 1>;

	enum class IOFormat {
		Pretty,
		SageMath,
		PARI,
		LaTeX,
	};

	void format_eigen(std::ostream &o, const mpz_vector& v, IOFormat style);

	namespace internal
	{
		std::strong_ordering operator<=>(const mpz_vector& a, const mpz_vector& b);
	}
}

#include "common.tpp"
