/* SPDX-FileCopyrightText: © 2022 Maik Herbers */
/* SPDX-License-Identifier: GPL-3.0-or-later */

#pragma once

#include <iostream>
#include <map>
#include <set>

#include "common.hpp"
#include "cyclotomic_field.hpp"
#include "sl2z.hpp"

namespace weil
{
	struct PowerSeries
	{
		void increment(uint64_t idx);

		mpq_class offset;
		std::vector<mpz_class> coeffs;
	};

	void format_powerseries(std::ostream& o, const PowerSeries& p, IOFormat style);

	struct VVThetaSeries
	{
		std::map<mpz_vector, PowerSeries> components;
	};

	void format_vvthetaseries(std::ostream& o, const VVThetaSeries& T, IOFormat style);

	struct Orbit
	{
		mpz_vector representative;
		uint64_t length;
	};

	class DiscriminantForm
	{
	      public:
		DiscriminantForm(const mpz_matrix& g);

		/*
		 * Calculate the quadratic form and bilinear form of the discriminant form
		 */
		mpq_class q(const mpz_vector& v) const;
		mpq_class b(const mpz_vector& v, const mpz_vector& w) const;
		mpq_class qc(const mpz_vector& v, const mpz_class& c, const mpz_vector& xc) const;

		/*
		 * Action of multiplication by `c'
		 */
		std::set<mpz_vector> c_kernel(const mpz_class& c) const;
		std::set<mpz_vector> c_image(const mpz_class& c) const;
		std::set<mpz_vector> c_star(const mpz_class& c) const;

		mpz_class element_order(const mpz_vector& v) const;
		mpz_class level() const;

		mpz_vector xc(const mpz_class& c) const;

		/*
		 * Calculate the first prec + 1 terms of the Fourier expansion
		 * of the theta series, i.e.
		 *           q^offset (∑_{i = 0}^{prec} a_i q^i).
		 * While `theta_series' computes and outputs the expansion for
		 * every component, `theta_series_direct' does so for only one
		 * representative of each orbit under Aut.
		 */
		VVThetaSeries theta_series(uint64_t prec) const;
		VVThetaSeries theta_series_direct(uint64_t prec, const std::vector<mpz_matrix>& Aut) const;

		mpz_matrix get_gram_wrt_smith_basis() const;

		std::pair<std::vector<Orbit>, std::map<mpz_vector, mpz_vector>> get_orbits(
		  const std::vector<mpz_matrix>& Aut) const;

		CyclotomicField_matrix transformation_matrix(const std::vector<mpz_matrix>& Aut, const SL2Z& M) const;

		/*
		 * Iterate over the elements of the discriminant form
		 */
		class Iterator
		{
		      public:
			using iterator_category = std::input_iterator_tag;
			using value_type = mpz_vector;
			using reference = mpz_vector&;
			using pointer = mpz_vector*;

			Iterator(mpz_class counter_, mpz_class max_, const mpz_vector& elem_divs_);

			value_type operator*() const;
			DiscriminantForm::Iterator& operator++();
			DiscriminantForm::Iterator& operator++(int);
			bool operator==(const DiscriminantForm::Iterator& other) const;

		      private:
			void increment();

			const mpz_vector elem_divs;
			mpz_class counter;
			const mpz_class max;
			const int64_t n;
			mpz_vector v;
		};

		Iterator begin() const;
		Iterator end() const;

		mpz_class cardinality;

	      private:
		mpq_matrix gram_matrix;
		mpz_vector elem_divs;
	};
}
