;;; SPDX-FileCopyrightText: © 2022 Maik Herbers
;;; SPDX-License-Identifier: GPL-3.0-or-later

(use-modules (gnu packages algebra)
             (gnu packages base)
             (gnu packages commencement)
             (gnu packages guile)
             (gnu packages linux)
             (gnu packages multiprecision)
             (gnu packages pkg-config)

             (guix build-system gnu)
             (guix gexp)
             (guix licenses)
             (guix utils)
             (guix packages)

             (ice-9 popen)
             (ice-9 rdelim))

(define weil
  (let*
      ((%source-dir (dirname (current-filename)))
       (cmd (string-append "git -C " %source-dir " describe --always"))
       (port (open-pipe cmd OPEN_READ))
       (%git-version (read-line port))
       (select? (lambda (name _)
                  (not (string=? name
                                 (string-append %source-dir "/.git"))))))
    (close port)
    (package
      (name "weil")
      (version %git-version)
      (source (local-file %source-dir
                          #:recursive? #t
                          #:select? select?))
      (build-system gnu-build-system)
      (arguments
       (list #:tests? #f
             #:make-flags #~(list (string-append "DESTDIR=" #$output)
                                  "PREFIX="
                                  (string-append "CC=" #$(cc-for-target))
                                  (string-append "CXX=" #$(cxx-for-target))
                                  (string-append "LDFLAGS_EXTRA=-Wl,-rpath," #$output "/lib"))
             #:phases #~(modify-phases %standard-phases
                          (delete 'configure)
                          (add-after 'install 'wrap-script
                            (lambda _
                              (wrap-script (string-append #$output "/bin/weil-get-automorphisms.sh")
                                '("PATH" prefix (#$(file-append coreutils "/bin")
                                                 #$(file-append gap"/bin")
                                                 #$(file-append pari-gp "/bin")
                                                 #$(file-append sed "/bin")
                                                 #$(file-append util-linux "/bin")))))))))
      (inputs (list eigen
                    gmp
                    guile-3.0 ;; for wrap-script
                    gap
                    pari-gp
                    pkg-config))
      (synopsis "Computing with the Weil-Representation of lattices")
      (description synopsis)
      (home-page (string-append "file://" %source-dir))
      (license gpl3+))))

(package-with-c-toolchain weil `(("toolchain" ,gcc-toolchain-12)))
