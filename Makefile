# SPDX-FileCopyrightText: © 2022 Maik Herbers
# SPDX-License-Identifier: GPL-3.0-or-later

.SUFFIXES:

progname      = weil
libname       = libweil
version-major = 0
version-minor = 1
soname        = $(libname).so.$(version-major)
lib_interface = common cyclotomic_field discriminant_form pari sl2z
lib_objs      = atomic_theta_helper common cyclotomic_field discriminant_form pari sl2z
prog_objs     = weil
script_files  = weil-get-automorphisms.sh

pkg-config-deps = gmpxx eigen3

lib_extra_libs = -lpari

DESTDIR    =
PREFIX     = /usr/local
BINDIR     = $(DESTDIR)$(PREFIX)/bin
LIBDIR     = $(DESTDIR)$(PREFIX)/lib
INCLUDEDIR = $(DESTDIR)$(PREFIX)/include

CC  = gcc
CXX = g++
LD  = $(CXX)

CFLAGS   = -pipe -std=c18 -Wall -Wextra -Wpedantic -Wshadow -g -O2 -fPIC
CXXFLAGS = -pipe -std=c++20 -Wall -Wextra -Wpedantic -Wshadow -g -O2 -fPIC -fopenmp \
           $(shell pkg-config --cflags-only-other $(pkg-config-deps))
CPPFLAGS = -DWEIL_VERSION='"$(version-major).$(version-minor)"' -iquote include \
           $(shell pkg-config --cflags-only-I $(pkg-config-deps))
LDFLAGS  = -fopenmp -Llib $(LDFLAGS_EXTRA) \
           $(shell pkg-config --libs-only-L --libs-only-other $(pkg-config-deps))
LDLIBS   = -lweil \
	   $(shell pkg-config --libs-only-l $(pkg-config-deps))

.PHONY: all
all: setup compile

.PHONY: setup
setup: build lib out

out build lib:
	mkdir -p $@

.PHONY: clean
clean: $(addprefix clean/,build lib out)

clean/%:
	@printf '\tCLEAN\t%s\n' $*
	@$(RM) -- $(wildcard $*/*)

compile: $(addprefix out/,$(progname))  \
         $(addprefix lib/,$(soname) \
                          $(libname).a)

build/%.o: src/%.c
	@printf '\tCC\t%s\n' build/$*.o
	@$(CC) $(CPPFLAGS) $(CFLAGS) -c -o $@ $^

build/%.o: src/%.cpp
	@printf '\tCXX\t%s\n' build/$*.o
	@$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c -o $@ $^

out/$(progname): .EXTRA_PREREQS = lib/$(soname)
out/$(progname): $(patsubst %,build/%.o,$(prog_objs))
	@printf '\tLD\t%s\n' $@
	@$(LD) $(LDFLAGS) $^ $(LDLIBS) -o $@

lib/$(soname): $(patsubst %,build/%.o,$(lib_objs))
	@printf '\tLD\t%s\n' $@
	@$(LD) -Wl,-soname,$(soname) -shared -o $@ $^ $(lib_extra_libs)
	@ln -sf $(soname) lib/$(libname).so

lib/$(libname).a: $(patsubst %,build/%.o,$(lib_objs))
	@printf '\tAR\t%s\n' $@
	@$(AR) rcs $@ $^

install-header/%:
	@printf '\tINSTALL\t%s\n'  $*
	@install -Dm644 include/$* $(INCLUDEDIR)/weil/$*

install-script/%:
	@printf '\tINSTALL\t%s\n'  $*
	@install -Dm755 scripts/$* $(BINDIR)/$*

.PHONY: install
install: $(patsubst %,install-header/%.hpp,$(lib_interface)) \
         $(patsubst %,install-script/%,$(script_files))
	@printf '\tINSTALL\t%s\n' $(progname)
	@install -Dm755 out/$(progname) $(BINDIR)/$(progname)

	@printf '\tINSTALL\t%s\n' $(soname)
	@install -Dm755 lib/$(soname) $(LIBDIR)/$(soname)
	@ln -s $(LIBDIR)/$(soname) $(LIBDIR)/$(libname).so

	@printf '\tINSTALL\t%s\n' $(libname).a
	@install -Dm644 lib/$(libname).a $(LIBDIR)/$(libname).a

	@sed 's#@prefix@#$(DESTDIR)$(PREFIX)#; s/@version@/$(version-major).$(version-minor)/' $(libname).pc.in >$(libname).pc
	@install -Dm644 $(libname).pc $(LIBDIR)/pkgconfig/$(libname).pc
